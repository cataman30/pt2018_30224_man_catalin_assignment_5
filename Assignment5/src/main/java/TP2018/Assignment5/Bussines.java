package TP2018.Assignment5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Bussines {
	
	
	public static ArrayList<MonitoredData> readFromFile(String filePath)
	{
		Stream<String> stream = null;
		
		try 
		{
			stream = Files.lines(Paths.get(filePath));				
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		return (ArrayList<MonitoredData>) stream.map(line -> line.split("		")).map(fragment -> {return new MonitoredData(fragment[0], fragment[1], fragment[2]);}).collect(Collectors.toList());
		
	}
	
	public static int countDays(ArrayList<MonitoredData> md)
	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		int nr = (int) md.stream().map(x -> df.format(x.getStartTime().getTime())).distinct().count();
		
		return nr;
		
	}
	
	public static Map<String, Integer> countActivityAparritions(ArrayList<MonitoredData> md)
	{
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		md.stream().map(x -> x.getActivity()).forEach(x -> {
			
			if(map.containsKey(x))
				map.put(x, map.get(x).intValue()+1);
			else
				map.put(x, 1);
			
		});
		
		return map;
	}
	
	public static Map<String, Map<String, Integer>> countActivityAparritionsPerDay(ArrayList<MonitoredData> md)
	{
		
		Map<String, Map<String, Integer>> map = new HashMap<String, Map<String, Integer>>();
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		map = md.stream().collect(Collectors.groupingBy(x -> df.format(x.getStartTime().getTime()),
							Collectors.groupingBy(y -> y.getActivity(),
							Collectors.summingInt(z -> 1))));
		
		return map;
	}
	
	public static Map<String, Double> countTimesPerActivity(ArrayList<MonitoredData> md)
	{
			Map<String, Double> map = new HashMap<String, Double>(); 
			Map<String, Double> mapAux = new HashMap<String, Double>(); 
		
			md.stream().map(x -> x.getActivity()).distinct().forEach(x -> {
				
				mapAux.put(x, (double) 0);
			});
			
			md.stream().forEach(x -> {
				mapAux.put(x.getActivity(), mapAux.get(x.getActivity()).doubleValue() + (x.getEndTime().getTimeInMillis() - x.getStartTime().getTimeInMillis())/60000);
			});
			
			map = mapAux.entrySet().stream().filter(x -> x.getValue() > 600).collect(Collectors.toMap(y -> y.getKey(), z -> z.getValue()));
		
		return map;
	}
	
	public static ArrayList<String> checkPercentages(ArrayList<MonitoredData> md)
	{
		Map<String, Integer> map;
		Map<String, Integer> map2;
		ArrayList<String> al = new ArrayList<String>();
		
		map = md.stream().collect(Collectors.groupingBy(x -> x.getActivity(), Collectors.summingInt(y -> 1)));
		map2 = md.stream().filter(x -> (x.getEndTime().getTimeInMillis() - x.getStartTime().getTimeInMillis())/60000 < 5)
				.collect(Collectors.groupingBy(x -> x.getActivity(), Collectors.summingInt(y -> 1)));
		
		al = (ArrayList<String>) map2.keySet().stream().filter(x -> (map2.get(x)*100/map.get(x) >= 90)).collect(Collectors.toList());
		
		return al;
	}
	
}
