package TP2018.Assignment5;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

public class App 
{
	
	private static void printOcurrencesOfActionsInLog(String nameOfFile, Map<String, Integer> map)
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter(nameOfFile, "UTF-8");
		
			map.keySet().forEach(x -> writer.println(x+" "+map.get(x)));
			
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	private static void printActivityCountPerDay(String nameOfFile, Map<String, Map<String, Integer>> map)
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter(nameOfFile, "UTF-8");
		
			map.keySet().stream().sorted().forEach(x ->{
	    		
				writer.println("\n\n\n"+x+"\n\n\n");
	    		
	    		map.get(x).keySet().stream().forEach(y ->{
	    			
	    			writer.println(y + " " +map.get(x).get(y));
	    			
	    		});
	    		
	    	});
			
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	private static void printLargerThanTenHours(String nameOfFile, Map<String, Double> map)
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter(nameOfFile, "UTF-8");
		
			map.keySet().forEach(x -> {
	    		writer.println(x + " " + map.get(x));
	    	});
			
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	private static void printGreaterThan90Percent(String nameOfFile, ArrayList<String> activities)
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter(nameOfFile, "UTF-8");
		
			activities.stream().forEach(x -> writer.println(x));
			
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		
	}
	
    public static void main( String[] args )
    {
    	ArrayList<MonitoredData> list = Bussines.readFromFile("C:\\Users\\Administrator\\Desktop\\Activities.txt");
    	
    	//list.forEach(x -> System.out.println(x));
    	
    	System.out.println(Bussines.countDays(list));
    	
    	printOcurrencesOfActionsInLog("OcurrencesOfActions.rtf", Bussines.countActivityAparritions(list)); 
    	printActivityCountPerDay("ActivityCountPerDay.rtf", Bussines.countActivityAparritionsPerDay(list));
    	printLargerThanTenHours("LargerThanTenHours.rtf", Bussines.countTimesPerActivity(list));
    	printGreaterThan90Percent("GreaterThan90Percent.rtf", Bussines.checkPercentages(list));
    }
}
