package TP2018.Assignment5;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MonitoredData {

	private Calendar startTime = Calendar.getInstance();
	private Calendar endTime = Calendar.getInstance();
	private String activity;
	
	public MonitoredData(String start, String end, String activity) {
		
		super();
		
		DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
			try 
			{
				this.startTime.setTime(f.parse(start));
				this.endTime.setTime(f.parse(end));
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
				
		this.activity = activity;
	}

	public Calendar getEndTime() {
		return endTime;
	}
	
	public Calendar getStartTime() {
		return startTime;
	}

	public String getActivity() {
		return activity;
	}
	
	@Override
	public String toString()
	{
		DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		StringBuilder sb = new StringBuilder();
		
		sb.append(f.format(startTime.getTime()).toString()+" ");
		sb.append(f.format(endTime.getTime()).toString()+" ");
		sb.append(activity);
		
		return sb.toString();
	}
	
}
